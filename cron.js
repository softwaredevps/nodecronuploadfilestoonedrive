

const cron = require('node-cron');


const readline = require('readline');
const { google } = require('googleapis');

const fs = require('fs');
const request = require('request');
const https = require('https');
const { v4: uuidv4 } = require('uuid');

const onedrive_folder = 'testFolder'; // Folder name on OneDrive

const spreadsheetId = '1_MIePHCooigU-w5rCjqisjz30WWQsh8m9op50vRu_CQ';
const redirect_uri = 'http://localhost/dashboard';
const client_id = "c70fb693-8f0d-423c-9949-033faf926f70";
const client_secret = "KOw/YO9hBcp12o@fZCjgN_N-WkKtFWm7";
const refresh_token = "MCYnZiLjTUkFJdeu63qQh0pwdV74yb3NR6WFRujjLBQZPXl4wkcFgYATrowipxWbA9yfn0YeiDcQJF5i6lN8*SppI4nYYY*CoH1sTGi4AUQaHOIVwPa*vK3NFFMowxJ5kca2MRjcbFBbj9lS6dqkUUTCjN4OYcv0Kmcbx*9cIIgQrnjuh0Ukg9KrtlbtiwzHhMQe0DlNQubsGAVH5YXnd7kyAusK1rFneOSFIZi9Ln7JgZ83li*u5eVdlytfr*txPWHn3AqXPdsj5ig!lsE5NYpmYxXGe5dNSshREPZobv*yA3bTkgzootpSr3XneXD2O6tT6yyvJakkq!2O6anNJY1S34biJeQl80XoegW!JS9LdUe!QxIGDqwT7ishrnTFNN2LzXxRrHlG8cA55bXd4z0c$";
const grant_type = 'refresh_token';

cron.schedule('*/5 * * * *', () => {

    const SCOPES = ['https://www.googleapis.com/auth/spreadsheets'];
    const TOKEN_PATH = 'token.json';

    fs.readFile('credentials.json', (err, content) => {
        if (err) return console.log('Error loading client secret file:', err);
        authorize(JSON.parse(content), listMajors);
    });

    authorize = (credentials, callback) => {
        const { client_secret, client_id, redirect_uris } = credentials.installed;
        const oAuth2Client = new google.auth.OAuth2(
            client_id, client_secret, redirect_uris[0]);

        fs.readFile(TOKEN_PATH, (err, token) => {
            if (err) return getNewToken(oAuth2Client, callback);
            oAuth2Client.setCredentials(JSON.parse(token));
            callback(oAuth2Client);
        });
    }

    getNewToken = (oAuth2Client, callback) => {
        const authUrl = oAuth2Client.generateAuthUrl({
            access_type: 'offline',
            scope: SCOPES,
        });
        console.log('Authorize this app by visiting this url:', authUrl);
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout,
        });
        rl.question('Enter the code from that page here: ', (code) => {
            rl.close();
            oAuth2Client.getToken(code, (err, token) => {
                if (err) return console.error('Error while trying to retrieve access token', err);
                oAuth2Client.setCredentials(token);
                fs.writeFile(TOKEN_PATH, JSON.stringify(token), (err) => {
                    if (err) return console.error(err);
                    console.log('Token stored to', TOKEN_PATH);
                });
                callback(oAuth2Client);
            });
        });
    }

    listMajors = (auth) => {
        const sheets = google.sheets({ version: 'v4', auth });
        sheets.spreadsheets.values.get({
            spreadsheetId: spreadsheetId,
            range: 'Sheet1!A2:H',
        }, async (err, res) => {
            if (err) return console.log('The API returned an error: ' + err);
            const rows = res.data.values;
            if (rows.length) {
                await Promise.all(rows.map(async (row, index) => {
                    const indx = index + 2
                    const range = "Sheet1!H" + indx
                    if (row[7] == 'new') {
                        let urls = [row[3], row[4], row[5], row[6]]
                        const subFolder = row[2]
                        urls.map((url, urlIndex) => {
                            if (url != '') {
                                const pieces = url.split(/[\s.]+/)
                                let extention = pieces[pieces.length - 1]
                                extention = extention == '' ? pieces[pieces.length - 2] : extention
                                newFileName = Date.now().toString()
                                const onedrive_filename = uuidv4() + '.' + extention
                                request.post({
                                    url: 'https://login.microsoftonline.com/common/oauth2/v2.0/token',
                                    form: {
                                        redirect_uri: redirect_uri,
                                        client_id: client_id,
                                        client_secret: client_secret,
                                        refresh_token: refresh_token,
                                        grant_type: grant_type
                                    },
                                }, function (error, response, body) {
                                    https.get(url, res => {
                                        const bufs = [];
                                        res.on('data', function (chunk) {
                                            bufs.push(chunk)
                                        });
                                        res.on('end', function () {
                                            const data = Buffer.concat(bufs);
                                            request.put({
                                                url: 'https://graph.microsoft.com/v1.0/drive/root:/' + onedrive_folder + '/' + subFolder + '/' + onedrive_filename + ':/content',
                                                headers: {
                                                    'Authorization': "Bearer " + JSON.parse(body).access_token,
                                                },
                                                body: data,
                                            }, function (er, re, bo) {
                                                // console.log("copy response", bo);
                                                console.log("File from Url " + url + " is successfully saved to folder " + subFolder + "with name " + onedrive_filename);
                                                // updating sheet status
                                                if(urlIndex == 3){
                                                    // setInterval(function () {
                                                        sheets.spreadsheets.values.update({
                                                            "spreadsheetId": spreadsheetId,
                                                            "range": range,
                                                            "valueInputOption": "USER_ENTERED",
                                                            "resource": {
                                                                "values": [['read']]
                                                            }
                                                        }, (err, response) => {
                                                            if (err) { console.log(err); }
                                                            else {
                                                                console.log("response", "response1")
                                                            }
                                                        });
                                                    // }, 1000);
                                                }
                                            });
                                        });
                                    });
                                });
                            }
                            else{
                                if(urlIndex == 3){
                                    // setInterval(function () {
                                        sheets.spreadsheets.values.update({
                                            "spreadsheetId": spreadsheetId,
                                            "range": range,
                                            "valueInputOption": "USER_ENTERED",
                                            "resource": {
                                                "values": [['read']]
                                            }
                                        }, (err, response) => {
                                            if (err) { console.log(err); }
                                            else {
                                                console.log("response", "response2")
                                            }
                                        });
                                    // }, 1000);
                                }
                            }
                        })
                    } else {
                        console.log("No data found with status-new for row index", index)
                    }
                }));
            } else {
                console.log('No data found in sheet.');
            }
        });
    }

});
